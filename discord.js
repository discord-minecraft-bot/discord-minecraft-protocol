#!/usr/bin/env node

const { Client, Intents } = require('discord.js');
const { token } = require("./config.json");

// Create a new client instance
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
  ]
});

// When the client is ready, run this code (only once)
client.once('ready', () => {
  console.info('Discord Ready!');
  // Setting Status
  client.user.setActivity("I connect a minecraft server chat to a discord server chat");
});

client.once('shardDisconnect', (event, id) => {
  console.error("Discord Disconnected");
  console.error("Event:", event, "ID:", id);
  process.exit(1);
});

// Login to Discord with your client's token
client.login(token);

module.exports = client;
