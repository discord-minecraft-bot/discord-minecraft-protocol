#!/usr/bin/env node
const { username, host, port } = require('./config.json');
var mc = require('minecraft-protocol');

var client = mc.createClient({
  host: host,  // optional
  port: port,         // optional
  username: username,
});

client.on('connect', function () {
  console.info('Minecraft Ready!');
});
client.on('end', function (packet) {
  console.error('Minecraft Ended!');
  process.exit(1);
});

module.exports = client;
