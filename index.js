#!/usr/bin/node
// Require the necessary discord.js classes
const minecraft = require('./minecraft.js');
const discord = require('./discord.js');
const { channelID } = require('./config.json');
const fs = require('node:fs')
const path = require('node:path')
const { Collection } = require('discord.js')

// Creating channels.json
try {
  fs.readFileSync('channels.json');
} catch (err) {
  // Do something
  let emptyChannels = {
    "guildIds": [],
    "channelIds": [],
    "hosts": new Array(),
    "ports": new Array()
  }
  fs.writeFileSync('channels.json', JSON.stringify(emptyChannels, null, 2));
}
// Function to delete deleted channels
function delete_channel(channel) {
  const { channelIds, guildIds, hosts, ports } = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));

  // Removing ID on the list
  // https://stackoverflow.com/questions/5767325/how-can-i-remove-a-specific-item-from-an-array
  const index = channelIds.indexOf(channel);
  if (index > -1) {
    channelIds.splice(index, 1); // 2nd parameter means remove one item only
    guildIds.splice(index, 1); // 2nd parameter means remove one item only
    [host] = hosts.splice(index, 1);
    [port] = ports.splice(index, 1);
  }
  // channelIds.filter((value, i) => i != index)
  // guildIds.filter((value, i) => i != index)
  let newChannels = { guildIds, channelIds, hosts, ports };
  fs.writeFileSync('./channels.json', JSON.stringify(newChannels, null, 2));
  console.log(`Deleting ${channel} as it doesn't exist.`)
}

discord.on('messageCreate', async (message) => {
  // Listen for chat messages from discord and echo them to minecraft
  // Do nothing if message coming from it self
  if (message.author.id == discord.user.id) return;
  if (message.channelId != channelID) return;

  // Formatting and printing message
  let sentMessage = "[" + message.author.username + "] " + message.content;
  console.log(sentMessage);

  // Sending it to minecraft
  await minecraft.write('chat', { message: `/tellraw @a \"${sentMessage}\"` });
});

// Bot Commands
// Registering Commands
discord.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  const command = require(filePath);
  // Set a new item in the Collection
  // With the key as the command name and the value as the exported module
  discord.commands.set(command.data.name, command);
}

discord.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return;

  const command = discord.commands.get(interaction.commandName);

  if (!command) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
  }
});

minecraft.on('chat', async function(packet) {
  // Listen for chat messages from minecraft and echo them to discord
  var jsonMsg = JSON.parse(packet.message);
  translate = jsonMsg.translate;

  // Do nothing if message coming from it self
  var username = jsonMsg.with[0].text;
  if (username === minecraft.username) return;

  // Sorting out types of meesages
  if (translate == 'chat.type.announcement') {
    // Getting message metadata
    var msg = jsonMsg.with[1].text;
    // Formatting message
    var sentMessage = `[${username}] ${msg}`;
  }
  else if (translate == 'chat.type.text'){
    // Getting message metadata
    var msg = jsonMsg.with[1].text;
    // Formatting message
    var sentMessage = `<${username}> ${msg}`;
  } else if (translate == "multiplayer.player.left") {
    // Formatting message
    var sentMessage = `${username} left the game`;
  } else if (translate == "multiplayer.player.joined") {
    // Formatting and printting message
    var sentMessage = `${username} joined the game`;
  } else if (/^chat.type.advancement/.test(translate)) {
    let advancementType = translate.split(".").pop()
    let advancmentArray = jsonMsg.with[1].with[0].translate.split(".");
    let advancmentTitle = advancmentArray[advancmentArray.length - 2];

    // Formatting and printting message
    if (advancementType == "challenge") {
      var sentMessage = `${username} has completed the challenge [${advancmentTitle}]`;
    } else if (advancementType == "task") {
      var sentMessage = `${username} has made the advancement [${advancmentTitle}]`;
    }
  } else {
    return;
  }

  // Logging and sending it to discord
  console.log(sentMessage)
  if (discord.isReady) await discord.channels.cache.get(channelID).send(sentMessage);
});
