const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js')
const fs = require('node:fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('get-servers')
    .setDescription('Gets all servers the bot is curretly relaying message to'),
  async execute(interaction) {
    const { channelIds, guildIds, hosts, ports } = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));

    // Getting all the channel in the guild
    let guildId = interaction.guildId;
    let channelList = new Array();
    let hostList = new Array();
    let portList = new Array();
    for (var i in channelIds) {
      if (guildIds[i] == guildId) {
        channelList.push("<#" + channelIds[i] + ">");
        hostList.push(hosts[i]);
        portList.push(ports[i]);
      }
    }
    let channelValue = channelList.join("\n");
    let hostValue = hostList.join("\n");
    let portValue = portList.join("\n");
    if (!channelValue) {
      channelValue = "No channels found!";
      hostValue = "-";
      portValue = "-";
    }

    // Setting up embed
    const channelEmbed = new MessageEmbed()
      .setTitle("Registered servers in the server")
      .addField("Channels:", channelValue)
      .addField("Hosts", hostValue, true)
      .addField("Ports:", portValue, true);
    // Reply with channels
    await interaction.reply({ embeds: [channelEmbed] });
  },
};
