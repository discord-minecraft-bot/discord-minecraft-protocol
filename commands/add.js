const { SlashCommandBuilder } = require('@discordjs/builders');
const fs = require('node:fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('add-server')
    .setDescription('Adds a minecraft server to relay message to')
    .addStringOption(option =>
      option.setName('host')
        .setDescription('The IP of the server to add')
        .setRequired(true))
    .addIntegerOption(option =>
      option.setName('port')
        .setDescription('The port number of the server')
        .setRequired(true))
    .addChannelOption(option =>
      option.setName('channel')
        .setDescription('The channel the messages are relayed to, defaults to current channel if not specified')
        .setRequired(false)),
  async execute(interaction) {
    const { channelIds, guildIds, hosts, ports } = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
    // Getting the guild and channel ID to add
    let optionChannel = interaction.options.get('channel');
    if (!optionChannel) {
      channel = interaction.channelId;
      guild = interaction.guildId;
    }
    else {
      channel = optionChannel.channel.id;
      guild = optionChannel.channel.guild.id;
    }
    let host = interaction.options.get('host')
    let port = interaction.options.get('port')

    // Checking if it is already on the list
    for (var id of channelIds) {
      if (channel == id) {
        await interaction.reply('Channel already in list. Skipping...');
        return;
      }
    }

    // Adding ID to the list
    channelIds.push(channel);
    guildIds.push(guild);
    hosts.push(host);
    ports.push(port);
    let newChannels = { guildIds, channelIds, hosts, ports };
    fs.writeFileSync('./channels.json', JSON.stringify(newChannels, null, 2));

    // Confirmation reply
    await interaction.reply(`<#${channel}> now echoing ${host}:${port}`);
  },
};
