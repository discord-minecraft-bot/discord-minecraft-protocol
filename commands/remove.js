#!/usr/bin/env node

const { SlashCommandBuilder } = require('@discordjs/builders');
const fs = require('node:fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('remove-channel')
    .setDescription('Unbind the channel from it\'s server')
    .addChannelOption(option =>
      option.setName('channel')
        .setDescription('Channel to unbind, defaults to current channel if not specified')
        .setRequired(false)),
  async execute(interaction) {
    const { channelIds, guildIds, hosts, ports } = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
    // Getting the guild and channel ID to add
    let option = interaction.options.get('channel');
    if (!option) {
      channel = interaction.channelId;
    } else {
      channel = option.channel.id;
    }

    // Checking if it is already on the list
    if (!channelIds.includes(channel)) {
      await interaction.reply('Channel not in list. Skipping...');
      return;
    }

    // Removing ID on the list
    // https://stackoverflow.com/questions/5767325/how-can-i-remove-a-specific-item-from-an-array
    const index = channelIds.indexOf(channel);
    if (index > -1) {
      channelIds.splice(index, 1); // 2nd parameter means remove one item only
      guildIds.splice(index, 1); // 2nd parameter means remove one item only
      [host] = hosts.splice(index, 1);
      [port] = ports.splice(index, 1);
    }
    // channelIds.filter((value, i) => i != index)
    // guildIds.filter((value, i) => i != index)
    let newChannels = { guildIds, channelIds, hosts, ports };
    fs.writeFileSync('./channels.json', JSON.stringify(newChannels, null, 2));

    // Confirmation reply
    await interaction.reply(`<#${channel}> no longer echoes ${host}:${port}`);
  },
};
