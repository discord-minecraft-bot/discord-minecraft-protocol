Discord bot that relays chat messages between a minecraft server and a discord server using minecraft protocol

# Notes
The bot that logs into minecraft currently doesn't uses an authentication. Hence, only works in offline servers. However, it can be setup to do so. For documentation on how to do that can be found at
[minecraft-protocol API doc](https://github.com/PrismarineJS/node-minecraft-protocol/blob/master/docs/API.md).

# Usage
## Config File
To use this a config.json file must be created. In the config.json there needs to be 5 elements. They are channelID, which is the channelID in which the bot will relay the minecraft messages to, token is the token for the discord bot, username is the username of the minecraft account that is going to be logging into the server, host is the hostname of the server and port is the port minecraft server is using. An example of a config.json file is found below.
``` json
{
    "channelID": "12345678",
    "token": "69abcd420",
    "username": "Discord",
    "host": "localhost",
    "port": 25565
}
```

## Installing Requirements
There is only 1 main requirements needed for this script to work, it is `discord.js` and `minecraft-protocol`. To install it do: 

``` sh
npm install discord.js minecraft-protocol
```

or if you are currently in the directory of the git repository

``` sh
npm install
```

## Running Bot
To start the discord bot simply run.

``` sh
node index.js
```

or if you are currently in the directory of the git repository

``` sh
npm run test
```

# TODO
- [ ] Make the mineraft bot reconnects after it got disconnected
- [ ] Add interaction to bot to setup which server it joins to
  - [ ] Add support for multiple bots
- [ ] Add support for multiple authentication methods for the minecraft bot
- [ ] Make advancements and challenges names "human readable"
